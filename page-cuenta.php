<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="shortcut icon" type="image/png" href="">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title><?php wp_title(); ?></title>
<?php 
wp_head(); 


?>
  </head>
<body <?php body_class(); ?>> <!-- https://developer.wordpress.org/reference/functions/body_class/ -->
 <main class="main account is-register">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-lg-5 account__main">
            <div class="account__main-wrap">
              <div class="account__logo"><a href="./">
                  <svg>
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#barker"></use>
                  </svg></a></div>
              <div class="account__message subtitle-xs">
                <h5>Bienvenido</h5>
              </div>
              <div class="account__options"><a class="is-active js-btn-form-type" href="" data-form="form-register">Registro</a><a class="js-btn-form-type" href="" data-form="form-login">Iniciar sesión</a></div>
              <div class="account__form-type js-account-form-type">
                <div class="account__form" id="form-register">
                  <p>Para poder registrarte rellena el formulario con tus datos. </p>
                  <form class="form" action="" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="rg-name" id="rg-name" placeholder="Nombres" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-apellidos" id="rg-apellidos" placeholder="Apellidos" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-phone" id="rg-phone" placeholder="Celular" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-email" id="rg-email" placeholder="Correo electrónico" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="password" name="rg-password" id="rg-password" placeholder="Contraseña" autocomplete="off">
                    </div>
                    <div class="field-action">
                      <button class="btn btn--primary btn--block" type="submit">Registrarme</button>
                    </div>
                    <div id="mess-reg">
                      <?php
                      if ( $_POST ) {
          
                              $error = 0;
                                      
                     
                              $username = $email = esc_sql($_REQUEST['rg-email']);
                              if ( !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $email) ) {  
                                
                                echo 'Please enter a valid email.';
                                $error = 1;
                              }
                              
                              if ( $error == 0 ) {
                                
                                $random_password = wp_generate_password( 12, false );  
                                $user_id = wp_create_user( $username, $_POST['rg-password'] , $email );  
                                wp_update_user( array( 'ID' => $user_id, 'first_name' => $_POST['rg-name'] ) );
                                update_user_meta( $user_id, 'billing_phone', esc_sql( $_POST['rg-phone'] ) );
                                if ( is_wp_error($user_id) ) {
                                
                                  echo 'Username already exists. Please try another one.';  
                                } else {
                                  
                                  $from     = get_option('admin_email');  
                                  $headers   = 'From: '.$from . "\r\n";  
                                  $subject   = "Registration successful";  
                                  $message   = "Registration successful.\nYour login details\nUsername: $username\nPassword: $random_password";  
                                  
                                  // Email password and other details to the user
                                  wp_mail( $email, $subject, $message, $headers );  
                                  
                                  echo "Please check your email for login details.";  
                                  
                                  $error = 2; // We will check for this variable before showing the sign up form. 
                                }
                              }
                     
                            }
                      ?>
                    </div>
                  </form>
                </div>
                <div class="account__form" id="form-login" style="display:none;">
                  <p>Ingresa poniendo tu correo y contraseña.</p>
                  <form name="loginform" id="loginform" class="form" action="<?php echo home_url();?>/wp-login.php" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="log" id="user_login" placeholder="Correo" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="password" name="pwd" id="user_pass" placeholder="Contraseña" autocomplete="off">
                    </div>
                    <div class="field-action">
                      <input type="hidden" name="redirect_to" value="<?php echo home_url();?>">
                      <button id="wp-submit" name="wp-submit" class="btn btn--primary btn--block" type="sumbit">Iniciar sesión</button>
                      <p class="account__call-reset u-text-center"><a class="js-btn-form-type" href="" data-form="form-reset">Olvide mi contraseña</a></p>
                    </div>
                    <div id="message"></div>
                  </form>
                </div>
                <div class="account__form" id="form-reset" style="display:none;">
                  <p>Ingresa tu correo para poder ayudarte.</p>
                  <form class="form" action="" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="rs-user-name" id="rs-user-name" placeholder="Correo" autocomplete="off">
                    </div>
                    <div class="field-action">
                      <button class="btn btn--primary btn--block" type="submit">Enviar</button>
                      <p class="account__call-reset u-text-center"><a class="js-btn-form-type" href="" data-form="form-login">Me acordé mi contraseña</a></p>
                    </div>
                  </form>
                </div>
              </div>
              <div class="account__register-options">
                <h5>También puedes registrarte con:</h5>
                <div class="account__register-option google"><a href="<?php echo home_url().'/wp-login.php?action=wordpress_social_authenticate&mode=login&provider=Google&redirect='.wsl_get_current_url(); ?>">Ingresa con Google</a></div>
                <div class="account__register-option facebook"><a href="<?php echo home_url().'/wp-login.php?action=wordpress_social_authenticate&mode=login&provider=Facebook&redirect='.wsl_get_current_url(); ?>">Ingresa con Facebook</a></div>
              </div>
            </div>
          </div>
          <div class="col-lg-7 account__bg u-padding--lv0 u-hidden-tablet-wide">
            <div class="account__bg-image"></div>
          </div>
        </div>
      </div>
    </main>

<?php wp_footer(); ?>
  </body>
</html>
