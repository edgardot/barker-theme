<?php
get_header();
?>
<div class="page-wrap">
      <div class="page-name">
        <h2>Producto</h2>
      </div>
      <main class="main">
        <section class="section products">
          <div class="container">
            <div class="section-header">
              <div class="title u-text-center">
                <h2>Nuestros productos</h2>
              </div>
            </div>
            <div class="products-carousel">
              <div class="owl-carousel js-products-carousel">
                <div class="product">
                  <figure class="product__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/pollo.png" alt=""></figure>
                  <div class="product__text">
                    <div class="product__title subtitle-sm">
                      <h2>Pollo</h2>
                    </div>
                    <p>Piezas de pollo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                  </div>
                  <div class="product__box">
                    <h4 class="product__box-title">Cuadro nutricional</h4>
                    <table class="product__table">
                      <thead>
                        <tr>
                          <th>Energía (Kcal, kg)</th>
                          <th>250</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>% Proteína, min</td>
                          <td>14.50</td>
                        </tr>
                        <tr>
                          <td>% Grasa, min</td>
                          <td>9.00</td>
                        </tr>
                        <tr>
                          <td>% Fibra, max</td>
                          <td>0.30</td>
                        </tr>
                        <tr>
                          <td>% Carbohidratos, min</td>
                          <td>6.00</td>
                        </tr>
                        <tr>
                          <td>% Calcio, min</td>
                          <td>0.90</td>
                        </tr>
                        <tr>
                          <td>% Fósforo, min</td>
                          <td>0.50</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="product">
                  <figure class="product__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/pavo.png" alt=""></figure>
                  <div class="product__text">
                    <div class="product__title subtitle-sm">
                      <h2>Pavo</h2>
                    </div>
                    <p>Piezas de pavo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                  </div>
                  <div class="product__box">
                    <h4 class="product__box-title">Cuadro nutricional</h4>
                    <table class="product__table">
                      <thead>
                        <tr>
                          <th>Energía (Kcal, kg)</th>
                          <th>250</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>% Proteína, min</td>
                          <td>14.50</td>
                        </tr>
                        <tr>
                          <td>% Grasa, min</td>
                          <td>9.00</td>
                        </tr>
                        <tr>
                          <td>% Fibra, max</td>
                          <td>0.30</td>
                        </tr>
                        <tr>
                          <td>% Carbohidratos, min</td>
                          <td>6.00</td>
                        </tr>
                        <tr>
                          <td>% Calcio, min</td>
                          <td>0.90</td>
                        </tr>
                        <tr>
                          <td>% Fósforo, min</td>
                          <td>0.50</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="product">
                  <figure class="product__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/res.png" alt=""></figure>
                  <div class="product__text">
                    <div class="product__title subtitle-sm">
                      <h2>Carne de Res</h2>
                    </div>
                    <p>Piezas de carne de res molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                  </div>
                  <div class="product__box">
                    <h4 class="product__box-title">Cuadro nutricional</h4>
                    <table class="product__table">
                      <thead>
                        <tr>
                          <th>Energía (Kcal, kg)</th>
                          <th>250</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>% Proteína, min</td>
                          <td>14.50</td>
                        </tr>
                        <tr>
                          <td>% Grasa, min</td>
                          <td>9.00</td>
                        </tr>
                        <tr>
                          <td>% Fibra, max</td>
                          <td>0.30</td>
                        </tr>
                        <tr>
                          <td>% Carbohidratos, min</td>
                          <td>6.00</td>
                        </tr>
                        <tr>
                          <td>% Calcio, min</td>
                          <td>0.90</td>
                        </tr>
                        <tr>
                          <td>% Fósforo, min</td>
                          <td>0.50</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/suscripcion' ?>">Diseña tu pedido</a></p>
          </div>
        </section>
      </main>
    </div>
    <?php
get_footer();
