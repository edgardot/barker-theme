var s,
accordion = {
	mediaQueries: {
		isMobile: window.matchMedia('(max-width: 767px)')
	},
	settings: {
		$body: 'body',
		arrowLeft: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M110.488 142.468L222.694 30.264c1.902-1.903 2.854-4.093 2.854-6.567s-.951-4.664-2.854-6.563L208.417 2.857C206.513.955 204.324 0 201.856 0c-2.475 0-4.664.955-6.567 2.857L62.24 135.9c-1.903 1.903-2.852 4.093-2.852 6.567 0 2.475.949 4.664 2.852 6.567l133.042 133.043c1.906 1.906 4.097 2.857 6.571 2.857 2.471 0 4.66-.951 6.563-2.857l14.277-14.267c1.902-1.903 2.851-4.094 2.851-6.57 0-2.472-.948-4.661-2.851-6.564L110.488 142.468z"/></svg>',
		arrowRight: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M222.701 135.9L89.652 2.857C87.748.955 85.557 0 83.084 0c-2.474 0-4.664.955-6.567 2.857L62.244 17.133c-1.906 1.903-2.855 4.089-2.855 6.567 0 2.478.949 4.664 2.855 6.567l112.204 112.204L62.244 254.677c-1.906 1.903-2.855 4.093-2.855 6.564 0 2.477.949 4.667 2.855 6.57l14.274 14.271c1.903 1.905 4.093 2.854 6.567 2.854 2.473 0 4.663-.951 6.567-2.854l133.042-133.044c1.902-1.902 2.854-4.093 2.854-6.567s-.945-4.664-2.847-6.571z"/></svg>'
	},

	init: function() {
		s = this.settings;
		this.bindEvents();
		this.accordion();
		this.stickyHeader();
		this.initBannerHomeCarousel();
		this.initTestimonyCarousel();
		this.initProductsCarousel();
		this.initBlogCarousel();
		this.initCategoriesBlogCarousel();
		this.initFunctionCarousel();
		this.initBenefitsCarousel();
		this.googleMapInitialize();
		this.showNavMobile();
		this.goToDown();
		this.changeTypeFormAccount();
		this.initHamburguerPopup();
		this.animateScrolling();
	},

	bindEvents: function() {
		
	},

	accordion: function(e) {
		const $accordion = $('.js-accordion');

		$.each($accordion, (i, el) => {
			let $elem = $(el);

			$('> .panel:eq(0) .panel__header', $elem).addClass('is-active').next().slideDown();

			$(`.panel__header`, $elem).on('click', (e) => {
				let $this = $(e.currentTarget);
				let dropDown = $this.closest('.panel').find('> .panel__content');

				$this.closest($elem).find('.panel > .panel__content').not(dropDown).slideUp();

				if ($this.hasClass('is-active')) {
					$this.removeClass('is-active');
				} else {
					$this.closest($elem).find('.panel__header.is-active').removeClass('is-active');
					$this.addClass('is-active');
				}

				dropDown.stop(false, true).slideToggle();

				e.preventDefault();	
			});
		});
	},

	stickyHeader: function () {
		const $siteNav = $('.site-nav');

		$(window).scroll(() => {
			var scrollTop = $(window).scrollTop();

			if (scrollTop >= 200) {
				$siteNav.addClass('is-sticky');
			} else {
				$siteNav.removeClass('is-sticky');
			}
		});
	},

	initBannerHomeCarousel: function() {
		// let isMobile = window.matchMedia('(max-width: 767px)');
		const $bannerHomeCarousel = $('.js-banner-home-carousel');

		$bannerHomeCarousel.on('initialize.owl.carousel', (event) => {
			accordion.changeImageBanner($bannerHomeCarousel);
		});

		$bannerHomeCarousel.owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 4000,
			animateOut: 'fadeOut',
			dots: false
		});

		accordion.mediaQueries.isMobile.addListener(accordion.changeImageBanner);
	},

	changeImageBanner: function(banner) {
		var $bannerItems = $(banner).find('.item');

		if (accordion.mediaQueries.isMobile.matches) {
			$.each($bannerItems, (i, el) => {
				let source = $(el).attr('style');
				
				if (source.indexOf('desktop') != -1) {
					let change = source.replace('desktop', 'mobile');
					$(el).attr('style', change);
				}				
			});
		} else {
			$.each($bannerItems, (i, el) => {
				let source = $(el).attr('style');
				
				if (source.indexOf('mobile') != -1) {
					let change = source.replace('mobile', 'desktop');
					$(el).attr('style', change);
				}				
			});
		}
	},

	initTestimonyCarousel: function () {
		const $testimonyCarousel = $('.js-testimony-carousel');

		$testimonyCarousel.owlCarousel({
			items: 1,
			autoHeight: true,
			loop: true,
			autoplay: true,
			autoplayTimeout: 6000
		});
	},

	initProductsCarousel: function() {
		const $productsCarousel = $('.js-products-carousel');

		$productsCarousel.owlCarousel({
			autoHeight: true,
			responsive: {
				0: {
					items: 1,
					dots: true
				},
				768: {
					items: 2,
					margin: 30,
					dots: true
				},
				992: {
					items: 3,
					margin: 25,
					dots: false
				}
			}
		});
	},

	initBlogCarousel: function () {
		const $blogCarousel = $('.js-blog-carousel');

		$blogCarousel.owlCarousel({
			items: 1,
			responsive: {
				992: {
					
				}
			}
		});
	},

	initCategoriesBlogCarousel: function() {
		var fixOwl = function(){
			var $stage = $('.owl-stage'),
					stageW = $stage.width(),
					$el = $('.owl-item'),
					elW = 0;
			$el.each(function() {
				elW += $(this).width()+ +($(this).css("margin-right").slice(0, -2))
			});
			if ( elW > stageW ) {
				$stage.width( elW );
			};
		}

		$('.js-categories-blog-carousel').owlCarousel({
			margin: 25,
			autoWidth: true,
			dots: false,
			navText: [s.arrowLeft, s.arrowRight],
			onInitialized: fixOwl,
			onRefreshed: fixOwl,
			responsive: {
				0: {
					nav: false,
					margin: 10
				},
				768: {
					items: 6,
					nav: true
				}
			}
		});
	},

	initFunctionCarousel: function() {
		const $functionCarousel = $('.js-function-carousel');
		const $functionItems = $('.js-function-items');

		$functionCarousel.owlCarousel({
			responsive: {
				0: {
					items: 1,
					dots: false
				},
				768: {
					items: 3,
					margin: 15
				},
				992: {
					items: 3,
					margin: 60
				}
			}
		});

		$functionCarousel.on('changed.owl.carousel', function (event) {
			let $tabs = $(event.target).parent().prev($functionItems);
			let index = (event.item.index + 1) - event.relatedTarget._clones.length / 2;
	
			$tabs.children().removeClass('is-active');
			$tabs.children().eq(index - 1).addClass('is-active');
		});

		$functionItems.children().on('click', (e) => {
			let $current = $(e.currentTarget);
			let id = $current.data('tab');
			
			$functionCarousel.trigger('to.owl.carousel', id - 1);

			$functionItems.children().removeClass('is-active');
			$current.addClass('is-active');
		});
	},

	initBenefitsCarousel: function() {
		const $benefitCarousel = $('.js-benefit-carousel');
		const $benefitItems = $('.js-benefit-items');

		$benefitCarousel.owlCarousel({
			items: 1,
			dots: false,
			loop: true,
			autoHeight: true,
			// onInitialized: setOwlStageHeight,
			// onResized: setOwlStageHeight,
			// onTranslated: setOwlStageHeight
		});

		// function setOwlStageHeight(event) {
		// 	var maxHeight = 0;
		// 	$('.owl-item.active').each(function () { // LOOP THROUGH ACTIVE ITEMS
		// 			var thisHeight = parseInt( $(this).height() );
		// 			maxHeight=(maxHeight>=thisHeight?maxHeight:thisHeight);
		// 	});
		// 	$('.owl-carousel').css('height', maxHeight );
		// 	$('.owl-stage-outer').css('height', maxHeight );
		// }

		$benefitCarousel.on('changed.owl.carousel', function (event) {
			let $tabs = $(event.target).parent().prev($benefitItems);
			let index = (event.item.index + 1) - event.relatedTarget._clones.length / 2;
	
			$tabs.children().removeClass('is-active');
			$tabs.children().eq(index - 1).addClass('is-active');
		});

		$benefitItems.children().on('click', (e) => {
			let $current = $(e.currentTarget);
			let id = $current.data('tab');
			
			$benefitCarousel.trigger('to.owl.carousel', id - 1);

			$benefitItems.children().removeClass('is-active');
			$current.addClass('is-active');
		});
	},

	googleMapInitialize: function() {
		$(window).on('load', () => {
			if (document.getElementById('map'))
				google.maps.event.addDomListener(window, 'load', accordion.googleMapConfig());
		});
	},

	googleMapConfig: function() {
		var locations = [];
		const $pointSale = $('.point-sale');

		$.each($pointSale, (i, el) => {
			let $el = $(el);
			let newItem = [];
			let locationData = $el.data('location');
			
			newItem = locationData.split(',');
			locations.push(newItem);
		});

		// console.log(locations);


		var punto = { lat: -33.92, lng: 151.25 };
		var image = {
			url: './images/icons/pin.png',
			scaledSize: new google.maps.Size(59, 67), // scaled size
		};

	  // var mapOptions = {
	  //   zoom: 10,
	  //   center: punto
	  // };

	  var map = new google.maps.Map(document.getElementById('map'), {
	  	mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
		

		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();

	  var marker, i;

	  for (i = 0; i < locations.length; i++) {
	  	marker = new google.maps.Marker({
	      position: new google.maps.LatLng(Number(locations[i][1]), Number(locations[i][2])),
	      map: map,
	      icon: image,
	      title: locations[i][0]
		  });

		  //extend the bounds to include each marker's position
  		bounds.extend(marker.position);
		  
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
	      return function() {
	        infowindow.setContent(locations[i][0]);
	        infowindow.open(map, marker);
	      }
	    })(marker, i));
	  }

	  map.fitBounds(bounds);
	},

	showNavMobile: function () {
		const $btnMenu = $('.js-btn-menu');
		const $btnBarkerNav = $('.js-btn-barker-nav');
		const isMobile = window.matchMedia('(max-width: 767px)');

		$btnMenu.on('click', (e) => {
			e.preventDefault();

			if ($(s.$body).hasClass('is-show-menu')) {
				$(s.$body).removeClass('is-show-menu is-show-phases-menu');
			} else {
				$(s.$body).addClass('is-show-menu');
			}
		});
		
		$btnBarkerNav.on('click', (e) => {
			if (isMobile.matches) {
				e.preventDefault();
	
				if ($(s.$body).hasClass('is-show-barker-menu')) {
					$(s.$body).removeClass('is-show-barker-menu');
				} else {
					$(s.$body).addClass('is-show-barker-menu');
				}
			}
		});
	},

	goToDown: function() {
		const $btnGoDown = $('.js-btn-godown');
		var $navbarHeight = $('.js-site-nav').outerHeight();

		$btnGoDown.on('click', (e) => {
			let $parent = $(e.currentTarget).closest('section');
			$('html, body').animate({
				scrollTop: $parent.next().offset().top - $navbarHeight
			}, 800);
		});
	},

	changeTypeFormAccount: function() {
		const $btnFormType = $('.js-btn-form-type');
		const $accountFormType = $('.js-account-form-type');

		$btnFormType.on('click', (e) => {
			e.preventDefault();
			let $this = $(e.currentTarget)
			let id = $this.data('form');
			console.log(id);
			
			switch (id) {
				case 'form-register':
					$('main').removeClass('is-register is-login is-reset');
					$('main').addClass('is-register');
					break;

				case 'form-login':
					$('main').removeClass('is-register is-login is-reset');
					$('main').addClass('is-login');
					break;
			
				case 'form-reset':
				$('main').removeClass('is-register is-login is-reset');
				$('main').addClass('is-reset');
			}

			$btnFormType.removeClass('is-active');
			$this.addClass('is-active');
			
			$accountFormType.children().hide();
			$(`#${id}`).show();


		});
	},

	initHamburguerPopup: function() {
		const $hamburguerPopup = $('.js-hamburguer-popup');

		$hamburguerPopup.on('click', (e) => {
			e.preventDefault();

			$.magnificPopup.open({
				closeBtnInside: true,
				closeOnBgClick: false,
				items: {
					src: '#barker-hamburguer',
					type: 'inline'
				},
				callbacks: {
					beforeOpen: function() {
						$('body').css('overflow', 'hidden');
					},
					beforeClose: function() {
						$('body').css('overflow', 'auto');
					}
				}
			});
		});
	},

	animateScrolling: function() {
		// Init ScrollMagic Controller
		var scrollMagicController = new ScrollMagic();

		var tween = TweenMax.staggerFromTo('.js-slide-toggle', 0.5,
			{
				width: '0%',
			},
			{
				width: '100%'
			},
			0.4
		);

		// Create the Scene and trigger when visible
		var scene = new ScrollScene({
			triggerElement: '#scene',
			duration: 500 /* How many pixels to scroll / animate */
		})
		.setTween(tween)
		.addTo(scrollMagicController);
		
		// Add debug indicators fixed on right side
		scene.addIndicators();
	}
}

export default accordion;