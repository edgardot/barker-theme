// import $ from 'jquery';
// window.jQuery = $;
// window.$ = $;

import 'owl.carousel';
import 'magnific-popup';
import accordion from './modules/accordion';
import delivery from './modules/delivery';
import Maintenance from './modules/maintenance';
import LocatePoint from './modules/localte-point';
import Profile from './modules/profile';

;(function ($) {

	accordion.init();
	delivery.init();

	new Maintenance();
	new LocatePoint();
	new Profile();

	/**
	 * Tabs
	 */
	const $tabs = $('.js-tabs');

	$.each($tabs, (i, el) => {
		let $tabCurrent = $(el);
		let $tabCurrentContent = $tabCurrent.next();
		let events = 'click';

		$tabCurrent.find('li').eq(0).addClass('is-active');

		$tabCurrentContent.children().hide();
		$tabCurrentContent.children().eq(0).show();

		if ($tabs.hasClass('hover')) {
			events += ' mouseover';
		} 
		console.log(events)

		$tabCurrent.find('li').on(events, (ev) => {
			let $current = $(ev.currentTarget);
			let id = $current.data('tab');

			$tabCurrent.find('li').removeClass('is-active');
			$current.addClass('is-active');

			$tabCurrentContent.children().hide();
			$tabCurrentContent.children(`#${id}`).show();
		});
	});
	
})(jQuery);
