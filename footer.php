
<!-- FOOTER -->
    <footer class="site-footer u-hidden-tablet">
      <div class="container">
        <div class="site-footer__header"><span class="u-flex">
            <svg>
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#barker"></use>
            </svg></span></div>
        <div class="row">
          <div class="col-lg-4">
            <h6 class="site-footer__title">MAPA DE SITIO</h6>
            <ul class="site-footer__links">
              <li><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a></li>
              <li><a href="<?php echo home_url().'/como-funciona' ?>">Como funciona</a></li>
              <li><a href="<?php echo home_url().'/sabores' ?>">Productos</a></li>
              <li><a href="<?php echo home_url().'/pedidos' ?>">Suscripción</a></li>
              <li><a href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></li>
            </ul>
          </div>
          <div class="col-lg-4">
            <h6 class="site-footer__title">CONTACTO</h6>
            <ul class="site-footer__links">
              <li><a href="mailto:<?php echo get_theme_mod('ars_mail',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#email"></use>
                    </svg></span>contacto@barker.pe</a></li>
              <li><a href="<?php echo get_theme_mod('bk_instagram',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#instagram"></use>
                    </svg></span>instagram</a></li>
              <li><a href="<?php echo get_theme_mod('bk_facebook',''); ?>"  target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#facebook"></use>
                    </svg></span>facebook</a></li>
              <li><a href="whatsapp://send?abid=+51<?php echo get_theme_mod('ars_whatsapp',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#phone"></use>
                    </svg></span>991750566</a></li>
            </ul>
          </div>
          <div class="col-lg-4">
            <h6 class="site-footer__title">CONSEJOS, RECETAS Y TIPS PARA TU MASCOTA</h6>
            <form action="" method="post">
              <div class="field-wrapper subscribe">
                <input type="text" name="email-subscribe" id="email-subscribe" placeholder="Ingresa tu correo">
                <button class="button icon" type="submit">ENVIAR</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </footer>
    <!-- /ENDS FOOTER-->
    <!-- /ENDS FOOTER-->
    <div class="hamburguer popup-inner" id="barker-hamburguer">
      <div class="hamburguer__content">
        <div class="subtitle-xs">
          <h3>¿Cuántas hamburguesas barker debes darle a tu perro?</h3>
        </div>
        <p>*Cada hamburguesa contiene 50 gramos de barker</p>
        <div class="hamburguer__table">
          <table>
            <thead>
              <tr>
                <th>PESO</th>
                <th>1.5m a 11m</th>
                <th>12m a 8 años</th>
                <th>8 años a más</th>
                <th>Gestante/lactante</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1 - 5 kg</td>
                <td>1</td>
                <td>2</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td>5 - 10 kg</td>
                <td>3</td>
                <td>4</td>
                <td>3</td>
                <td>6</td>
              </tr>
              <tr>
                <td>10 - 22 kg</td>
                <td>5</td>
                <td>8</td>
                <td>6</td>
                <td>12</td>
              </tr>
              <tr>
                <td>22 - 45 kg</td>
                <td>11</td>
                <td>16</td>
                <td>12</td>
                <td>24</td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="hamburguer__feedback">*Actividad Física intermedia. De ser el caso de perros de alta actividad, considerar duplicar o triplicar la ración de acuerdo a la exigencia física.</p>
      </div>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>