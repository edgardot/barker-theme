<?php
get_header();
?>
 <nav class="nav-secondary">
      <div class="nav-secondary__content">
        <div class="container u-flex"><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a><a href="<?php echo home_url().'/ingredientes' ?>">Ingredientes</a><a href="<?php echo home_url().'/como-funciona' ?>">Cómo funciona</a><a class="is-active" href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></div>
      </div>
    </nav>
    <div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Preguntas frecuentes</h2>
        </div>
        <section class="section faq">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-10">
                <div class="section-header">
                  <div class="title u-text-center">
                    <h2>¿Dudas? Revisa aquí</h2>
                  </div>
                  <p class="u-text-center">En los siguientes puntos podrás aclarar algunas dudas con respecto a nosotros en tres categorías: producto, distribución y suscripción.</p>
                </div>
              </div>
            </div>
            <div class="tab row u-pl-md--lv7 u-pr-md--lv7">
              <div class="faq__categories tabs-nav col-lg-2 js-tabs">
                <div class="subtitle-xs">
                  <h5>Categorías</h5>
                </div>
                <ul>
                  <?php
                  $cat = get_terms('categorias-preguntas',
                      array('hide_empty' => false,'parent'=>0,'orderby' => 'term_id','order'=> 'ASC'));
                  $i=1;
                  foreach($cat as $row){
                      echo '<li data-tab="'.$i.'"><span>'.$row->name.'</span></li>';
                      $tabContent.='<div class="tab__content-item" id="'.$i.'">
                  <ul class="accordion js-accordion">';
                      $res=query_posts(array('post_type' => 'preguntas-frecuentes', 'tax_query' => array( array( 'taxonomy' => 'categorias-preguntas','field' => 'id', 'terms' => array($row->term_id) ))) );
                      if(count($res)>0){
                        $b=1;
                        $sorted_res = array();
                          foreach($res as $resPost){
                              $ordr = get_field( 'orden', $resPost->ID );
                              $sorted_res[$ordr] = $resPost;
                          }
                          ksort($sorted_res);
                        foreach ($sorted_res as $resPost) { 
                        $tabContent.='<li class="panel"><a class="panel__header">
                        <span class="title">'.$b.'. '.$resPost->post_title.'</span><span class="action">
                          <svg>
                            <use xlink:href="'.get_template_directory_uri().'/assets/images/sprite.svg#arrow-down"></use>
                          </svg></span></a>
                      <div class="panel__content">
                        '.$resPost->post_content.'
                      </div>
                    </li>';
                        $b++;
                        }
                    }
                      $tabContent.='</ul>
                </div>';
                $i++;
                 }
                 
                  ?>
              </div>
              <div class="tab__content col-lg-10">
                <?php echo $tabContent;?>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
<?php
get_footer();