<div class="field-group culqi-payform-container col-1">
    <div class="field-wrapper">
        <label>Datos de facturación</label>
        <figure><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/tarjetas.png" alt=""></figure>
    </div>
    <div class="field-wrapper field-wrapper--simple culqi-payform-input">
        <input type="text" name="f-cardholder-name" id="f-cardholder-name" placeholder="Nombre del titular de la tarjeta">
    </div>
    <div class="field-wrapper field-wrapper--simple culqi-payform-input">
        <input type="text" name="f-cardholder-dni" id="f-cardholder-dni" placeholder="DNI del titular de la tarjeta">
    </div>
    <div class="field-wrapper field-wrapper--simple culqi-payform-subtitle culqi-payform-input">
        <label for="f-card-number">Número de tarjeta</label>
        <input type="text" name="f-card-number" id="f-card-number" placeholder="Ingrese los 14 dígitos de la tarjeta">
    </div>
    <div class="field-wrapper field-wrapper--simple culqi-payform-subtitle">
        <label>Fecha de vencimiento</label>
        <div class="row">
            <div class="col-md-6">
                <div class="select">
                    <select name="f-expiration-month-card" id="f-expiration-month-card">
                        <option value="">Mes</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="select">
                    <select name="f-expiration-year-card" id="f-expiration-year-card">
                        <option value="">Año</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="field-wrapper field-wrapper--simple culqi-payform-subtitle culqi-payform-input">
        <label>Código de seguridad</label>
        <input type="text" name="f-code-security" id="f-code-security" placeholder="CVC">
    </div>
<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order_2" value="Realizar Pedido" data-value="Realizar Pedido">Pagar</button>
</div>
