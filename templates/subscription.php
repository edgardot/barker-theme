<div class="content clearfix">
                    <!-- Step 1-->
                    <h3 id="delivery-wizard-h-0" tabindex="-1" class="title">
                        <span class="icon">
                            <svg>
                                <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-dog"></use>
                            </svg>
                        </span>
                        <span class="text">Perfil</span>
                    </h3>
                    <section id="delivery-wizard-p-0" role="tabpanel" aria-labelledby="delivery-wizard-h-0" class="body" aria-hidden="true" style="display: none;">
                        <div class="row justify-content-center">
                            <div class="col-lg-9">
                                <div class="u-text-center">
                                    <div class="subtitle-xs">
                                        <h5>¡Bienvenido al espacio de suscripciones Barker!</h5>
                                    </div>
                                    <div class="text-line">
                                        <p>Estás cada vez más cerca de darle a tu mascota lo que quiere y necesita. Sigue los pasos para realizar una suscripción Barker.</p>
                                        <p>Actualmente contamos con entregas a: Barranco, Chorrillos, Jesus Maria, Lince, La Molina, Miraflores, San Borja, San Isidro, San Miguel, Surco, Surquillo.</p>
                                        <p><span class="text-black">¿No encuentras tu distrito? Por favor déjanos</span><a class="link js-add-district-popup" href="">¡acá tu información!</a></p>
                                    </div>
                                    <div class="district-add popup-inner js-add-district">
                                        <div class="u-text-center">
                                            <div class="subtitle-xs">
                                                <h3>¿No encuentras tu distrito?</h3>
                                            </div>
                                            <p class="text-black">Por favor déjanos acá tu información</p>
                                        </div>
                                        <div class="form">
                                            <div class="field-wrapper">
                                                <input type="text" name="f-email-interested" id="f-email-interested" placeholder="Correo electrónico">
                                            </div>
                                            <div class="field-wrapper">
                                                <select class="js-add-district-select" name="f-district-search-add" id="f-district-search-add">
                                                    <option value=""></option>
                                                    <option value="Villa el Salvador">Villa el Salvador</option>
                                                    <option value="Villa María del Triunfo">Villa María del Triunfo</option>
                                                </select>
                                            </div>
                                            <div class="field-action u-flex justify-content-center">
                                                <button class="btn btn--secondary" type="button">Cancelar</button>
                                                <button class="btn btn--primary" type="button">Enviar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-line">
                                        <p class="text-black">Solo ayúdanos con los siguientes datos.</p>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight name-dog">
                                    <label for="f-dog-name">¿Cómo se llama tu perro?</label>
                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Ejemplo: Boby" autocomplete="off">
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>Sube la foto de <span class="js-dog-name">tu mascota</span></label>
                                    <p class="note u-text-center">(La foto es opcional)</p>
                                    <div class="file-upload">
                                        <div class="file-upload__preview">
                                            <figure><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/avatar-dog.png" alt=""></figure>
                                        </div>
                                        <div class="file-upload__field">
                                            <label class="btn btn--primary" for="f-input-file">Subir foto de <span class="js-dog-name">tu mascota</span></label>
                                            <input class="js-upload-hidden" type="file" name="f-input-file" id="f-input-file">
                                        </div>
                                    </div>
                                    <div class="feedback u-text-center">
                                        <p>Recuerda que el tamaño máximo de la imagen debe ser 8MB</p>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>¿Cuál es el sexo de <span class="js-dog-name">tu mascota</span>?</label>
                                    <div class="radio-group radio-group--square">
                                        <div class="radio">
                                            <input type="radio" name="f-dog-sex" id="f-male" value="Macho">
                                            <label for="f-male">Macho</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-dog-sex" id="f-female" value="Hembra">
                                            <label for="f-female">Hembra</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight filter-female">
                                    <label>Para un mejor filtro ayúdanos con las<br>siguientes opciones</label>
                                    <div class="radio-group radio-group--full">
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-pregnat" value="está preñada">
                                            <label for="f-pregnat">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p><span class="js-dog-name">tu mascota</span>, está preñada</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-pregnat"></label>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-has-pups" value="tiene crías">
                                            <label for="f-has-pups">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p><span class="js-dog-name">tu mascota</span>, tiene crías</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-has-pups"></label>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-status-na" value="na">
                                            <label for="f-status-na">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p>Ninguna de las anteriores</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-status-na"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2-->
                    <h3 id="delivery-wizard-h-1" tabindex="-1" class="title">
                        <span class="icon">
                            <svg>
                                <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-data"></use>
                            </svg>
                        </span>
                        <span class="text">Datos</span>
                    </h3>
                    <section id="delivery-wizard-p-1" role="tabpanel" aria-labelledby="delivery-wizard-h-1" class="body" aria-hidden="true" style="display: none;">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>¿Cuál es la edad de <span class="js-dog-name">tu mascota</span>?</label>
                                    <div class="radio-group radio-group--full row">
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-puppy" value="cachorro">
                                            <label for="f-puppy">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-puppy.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Cachorro</h6>
                                                        <p>0 a 1 año y medio</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-puppy"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-adult" value="adulto">
                                            <label for="f-adult">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-adult.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Adulto</h6>
                                                        <p>2 a 6 años</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-adult"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-older-adult" value="adulto mayor">
                                            <label for="f-older-adult">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-older-adult.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Adulto mayor</h6>
                                                        <p>7 a más años</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-older-adult"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label for="f-dog-name">¿Cúal es el peso de <span class="js-dog-name">tu mascota</span>?</label><span class="feedback">Elige entre las tres categorías de peso y usa las razas como ejemplo para guiarte</span>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-11">
                                            <div class="size-dog">
                                                <div class="size-dog__result">
                                                    <h5 class="js-dog-weight">Categoría 3 (10 - 22kg)</h5>
                                                    <span class="js-dog-race">Bulldog francés</span>
                                                    <figure class="js-dog-image"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/bulldog-francés.png" alt=""></figure>
                                                </div>
                                                <div class="size-dog__slider">
                                                    <div class="size-dog__less js-dog-less"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/jack-russell-terrier.png" alt=""></div>
                                                    <div class="size-dog__higher js-dog-higher"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/doberman.png" alt=""></div>
                                                    <div id="slider-size-dog" class="noUi-target noUi-ltr noUi-horizontal"><div class="noUi-base"><div class="noUi-connects"></div><div class="noUi-origin" style="transform: translate(-44.4444%, 0px); z-index: 4;"><div class="noUi-handle noUi-handle-lower" data-handle="0" tabindex="0" role="slider" aria-orientation="horizontal" aria-valuemin="0.0" aria-valuemax="100.0" aria-valuenow="55.6" aria-valuetext="5"></div></div></div><div class="noUi-pips noUi-pips-horizontal"><div class="noUi-marker noUi-marker-horizontal noUi-marker-large" style="left: 0%;"></div><div class="noUi-value noUi-value-horizontal noUi-value-large" data-value="0" style="left: 0%;">Menor</div><div class="noUi-marker noUi-marker-horizontal noUi-marker-large" style="left: 100%;"></div><div class="noUi-value noUi-value-horizontal noUi-value-large" data-value="9" style="left: 100%;">undefined</div></div></div>
                                                </div>
                                                <input type="hidden" name="dog-kg" id="dog-kg" value="Categoría 3 (10 - 22kg)">
                                                <input type="hidden" name="dog-race" id="dog-race" value="Bulldog francés">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label for="f-dog-name">Selecciona el nivel de actividad de <span class="js-dog-name">tu mascota</span></label>
                                    <div class="radio-group radio-group--full row">
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-low" value="bajo">
                                            <label for="f-low">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-bajo.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Bajo</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-low"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-medium" value="medio">
                                            <label for="f-medium">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-medio.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Medio</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-medium"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-high" value="alto">
                                            <label for="f-high">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/dog-alto.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Alto</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-high"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 3-->
                    <h3 id="delivery-wizard-h-2" tabindex="-1" class="title">
                        <span class="icon">
                            <svg>
                                <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-suscription"></use>
                            </svg>
                        </span>
                        <span class="text">Suscripción</span>
                    </h3>
                    <section id="delivery-wizard-p-2" role="tabpanel" aria-labelledby="delivery-wizard-h-2" class="body" aria-hidden="true" style="display: none;">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-lg-5 u-hidden-tablet">
                                <div class="delivery-flavors is-empty">
                                    <div class="delivery-flavors__feedback u-text-center">
                                        <h2>Aquí se mostrará el detalle de tus sabores seleccionados</h2>
                                    </div>
                                    <div class="delivery-flavors__carousel">
                                      <div class="owl-carousel owl-loaded owl-drag">
                                        <!-- Carrousel Description -->
                                                                                                                                                                                                        <!-- CodeHere -->
                                        <div id="barkerDescription-pollo" class="owl-stage-outer barker-suscription-item_description__disable">
                                            <div class="owl-stage">
                                                <div class="owl-item">
                                                    <figure class="delivery-flavor__image"><img src="https://demo.barker.pe/wp-content/uploads/2018/07/pollo.png" alt=""></figure>
                                                    <div class="delivery-flavor">
                                                        <div class="delivery-flavor__title subtitle-sm">
                                                            <h3>Pollo</h3>
                                                        </div>
                                                        <div class="delivery-flavor__content">
                                                            <p>Piezas de pollo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                                                        </div>
                                                        <div class="delivery-flavor__cta">
                                                            <a class="btn btn--primary btn--block" href="#">Ver cuadro nutricional</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                                                                                                                <!-- CodeHere -->
                                        <div id="barkerDescription-pavo" class="owl-stage-outer barker-suscription-item_description__disable">
                                            <div class="owl-stage">
                                                <div class="owl-item">
                                                    <figure class="delivery-flavor__image"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/pavo.png" alt=""></figure>
                                                    <div class="delivery-flavor">
                                                        <div class="delivery-flavor__title subtitle-sm">
                                                            <h3>Pavo</h3>
                                                        </div>
                                                        <div class="delivery-flavor__content">
                                                            <p>Piezas de pavo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                                                        </div>
                                                        <div class="delivery-flavor__cta">
                                                            <a class="btn btn--primary btn--block" href="#">Ver cuadro nutricional</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                                                                                                                <!-- CodeHere -->
                                        <div id="barkerDescription-carne-de-res" class="owl-stage-outer barker-suscription-item_description__disable">
                                            <div class="owl-stage">
                                                <div class="owl-item">
                                                    <figure class="delivery-flavor__image"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/res.png" alt=""></figure>
                                                    <div class="delivery-flavor">
                                                        <div class="delivery-flavor__title subtitle-sm">
                                                            <h3>Carne de Res</h3>
                                                        </div>
                                                        <div class="delivery-flavor__content">
                                                            <p>Piezas de carne de res molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>
                                                        </div>
                                                        <div class="delivery-flavor__cta">
                                                            <a class="btn btn--primary btn--block" href="#">Ver cuadro nutricional</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                                                                                                                <!-- CodeHere -->
                                        <div id="barkerDescription-mixto" class="owl-stage-outer barker-suscription-item_description__disable">
                                            <div class="owl-stage">
                                                <div class="owl-item">
                                                    <figure class="delivery-flavor__image"><img src="https://demo.barker.pe/wp-content/uploads/2018/08/pavo.png" alt=""></figure>
                                                    <div class="delivery-flavor">
                                                        <div class="delivery-flavor__title subtitle-sm">
                                                            <h3>Mixto</h3>
                                                        </div>
                                                        <div class="delivery-flavor__content">
                                                            <p>Mixto</p>
                                                        </div>
                                                        <div class="delivery-flavor__cta">
                                                            <a class="btn btn--primary btn--block" href="#">Ver cuadro nutricional</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                                                                         
                                        </div>                        
                                        <!-- </div> -->
                                        <!-- <div class="owl-carousel js-delivery-flavors-carousel">
                                            </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-7">
                                <!-- ////////////////////////////////////////////////// -->
                                <!-- Start Select Product-->
                                <!-- ////////////////////////////////////////////////// -->
                                <div class="delivery-filters">
                                    <div class="filter-by-flavors">
                                        <div class="subtitle-xs">
                                            <h4>Selecciona el sabor para <span class="js-dog-name">tu mascota</span></h4>
                                        </div>
                                        <p class="feedback"><span class="js-dog-name">tu mascota</span> necesita un estimado de <span class="js-kg-stimated"></span> kilogramos al día.</p>
                                        <!-- Sabores-->
                                        <div class="checkbox-group checkbox-group--circle">
                                            <!-- Circle Checkbox-->
                                                                                                                                                                                                                            <!-- CodeHere -->
                                            <div class="checkbox">
                                                <input type="radio" name="f-flavors" id="f-pollo-flavor" value="pollo" data-flavor="pollo" data-id="99">
                                                <label for="f-pollo-flavor">
                                                    <div class="checkbox__content">
                                                        <div class="checkbox__image">
                                                            <img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/pollo-icon.png" alt="">
                                                        </div>
                                                        <div class="checkbox__text">
                                                            <p>Pollo</p>
                                                        </div>
                                                    </div>
                                                <label class="checkbox__icon" for="f-chicken-flavor"></label>
                                                </label>
                                            </div>
                                                                                                                                                                                <!-- CodeHere -->
                                            <div class="checkbox">
                                                <input type="radio" name="f-flavors" id="f-pavo-flavor" value="pavo" data-flavor="pavo" data-id="150">
                                                <label for="f-pavo-flavor">
                                                    <div class="checkbox__content">
                                                        <div class="checkbox__image">
                                                            <img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/pavo-icon.png" alt="">
                                                        </div>
                                                        <div class="checkbox__text">
                                                            <p>Pavo</p>
                                                        </div>
                                                    </div>
                                                <label class="checkbox__icon" for="f-chicken-flavor"></label>
                                                </label>
                                            </div>
                                                                                                                                                                                <!-- CodeHere -->
                                            <div class="checkbox">
                                                <input type="radio" name="f-flavors" id="f-carne-de-res-flavor" value="carne-de-res" data-flavor="carne-de-res" data-id="144">
                                                <label for="f-carne-de-res-flavor">
                                                    <div class="checkbox__content">
                                                        <div class="checkbox__image">
                                                            <img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/carne-de-res-icon.png" alt="">
                                                        </div>
                                                        <div class="checkbox__text">
                                                            <p>Carne de Res</p>
                                                        </div>
                                                    </div>
                                                <label class="checkbox__icon" for="f-chicken-flavor"></label>
                                                </label>
                                            </div>
                                                                                                                                                                                <!-- CodeHere -->
                                            <div class="checkbox">
                                                <input type="radio" name="f-flavors" id="f-mixto-flavor" value="mixto" data-flavor="mixto" data-id="158">
                                                <label for="f-mixto-flavor">
                                                    <div class="checkbox__content">
                                                        <div class="checkbox__image">
                                                            <img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/icons/mixto-icon.png" alt="">
                                                        </div>
                                                        <div class="checkbox__text">
                                                            <p>Mixto</p>
                                                        </div>
                                                    </div>
                                                <label class="checkbox__icon" for="f-chicken-flavor"></label>
                                                </label>
                                            </div>
                                                                                                                                       
                                        </div>
                                    </div>
                                    <hr class="line-dotted line-dotted--yellow">
                                    <!-- ////////////////////////////////////////////////// -->
                                    <!-- Start Price Product-->
                                    <!-- ////////////////////////////////////////////////// -->
                                                                                                                                                                                    <!-- Start PriceBox -->
                                    <div id="barkerPriceBox-pollo" class="filter-by-time barker-suscription-item_priceBox__active">
                                        <div class="subtitle-xs">
                                            <h4>Elige tu suscripción</h4>
                                        </div>
                                        <div class="radio-group radio-group--check justify-content-center">
                                            <div class="radio column">
                                                                                                <input type="radio" name="f-suscription-time" id="f-one-week-pollo" value="1 semana" data-variation="137" data-quantity="1">
                                                <label for="f-one-week-pollo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title"></span>
                                                            <!-- CodeHere -->
                                                            <ul class="price">
                                                                <li>
                                                                    <span>
                                                                    S/.15.7 
                                                                    </span>
                                                                    <span>Por semana</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="radio-group radio-group--check owl-carousel js-suscription-type-carousel">
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-month-pollo" value="1 mes" data-variation="138" data-quantity="1">
                                                <label for="f-one-month-pollo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker x 1 mes</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.16.9  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                      
                                                <input type="radio" name="f-suscription-time" id="f-three-months-pollo" value="3 meses" data-variation="139" data-quantity="1">
                                                <label for="f-three-months-pollo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 3 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.15.8  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-three-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-six-months-pollo" value="6 meses" data-variation="140" data-quantity="1">
                                                <label for="f-six-months-pollo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 6 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.15.2  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-six-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-yearpollo" value="1 año" data-variation="141" data-quantity="1">
                                                <label for="f-one-yearpollo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 1 año</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.14.3  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-year">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="feedback">* Nota. La entrega del producto es semanal, en la fecha y el horario indicado según la disponibilidad de nuestros distribuidores en cada distrito.</p>
                                    </div>
                                    <!-- Ends PriceBox -->
                                                                                                                                                <!-- Start PriceBox -->
                                    <div id="barkerPriceBox-pavo" class="filter-by-time barker-suscription-item_priceBox__disable">
                                        <div class="subtitle-xs">
                                            <h4>Elige tu suscripción</h4>
                                        </div>
                                        <div class="radio-group radio-group--check justify-content-center">
                                            <div class="radio column">
                                                                                                <input type="radio" name="f-suscription-time" id="f-one-week-pavo" value="1 semana" data-variation="151" data-quantity="1">
                                                <label for="f-one-week-pavo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker por una semana</span>
                                                            <!-- CodeHere -->
                                                            <ul class="price">
                                                                <li>
                                                                    <span>
                                                                    S/.22.3 
                                                                    </span>
                                                                    <span>Por semana</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="radio-group radio-group--check owl-carousel js-suscription-type-carousel">
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-month-pavo" value="1 mes" data-variation="152" data-quantity="1">
                                                <label for="f-one-month-pavo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker x 1 mes</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.23.9  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                      
                                                <input type="radio" name="f-suscription-time" id="f-three-months-pavo" value="3 meses" data-variation="153" data-quantity="1">
                                                <label for="f-three-months-pavo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 3 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.22.5  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-three-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-six-months-pavo" value="6 meses" data-variation="154" data-quantity="1">
                                                <label for="f-six-months-pavo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 6 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.21.5  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-six-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-yearpavo" value="1 año" data-variation="155" data-quantity="1">
                                                <label for="f-one-yearpavo">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 1 año</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.20  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-year">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="feedback">* Nota. La entrega del producto es semanal, en la fecha y el horario indicado según la disponibilidad de nuestros distribuidores en cada distrito.</p>
                                    </div>
                                    <!-- Ends PriceBox -->
                                                                                                                                                <!-- Start PriceBox -->
                                    <div id="barkerPriceBox-carne-de-res" class="filter-by-time barker-suscription-item_priceBox__disable">
                                        <div class="subtitle-xs">
                                            <h4>Elige tu suscripción</h4>
                                        </div>
                                        <div class="radio-group radio-group--check justify-content-center">
                                            <div class="radio column">
                                                                                                <input type="radio" name="f-suscription-time" id="f-one-week-carne-de-res" value="1 semana" data-variation="145" data-quantity="1">
                                                <label for="f-one-week-carne-de-res">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker por una semana</span>
                                                            <!-- CodeHere -->
                                                            <ul class="price">
                                                                <li>
                                                                    <span>
                                                                    S/.18.5 
                                                                    </span>
                                                                    <span>Por semana</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="radio-group radio-group--check owl-carousel js-suscription-type-carousel">
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-month-carne-de-res" value="1 mes" data-variation="146" data-quantity="1">
                                                <label for="f-one-month-carne-de-res">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker x 1 mes</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.19.9  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                      
                                                <input type="radio" name="f-suscription-time" id="f-three-months-carne-de-res" value="3 meses" data-variation="147" data-quantity="1">
                                                <label for="f-three-months-carne-de-res">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 3 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.18.7  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-three-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-six-months-carne-de-res" value="6 meses" data-variation="148" data-quantity="1">
                                                <label for="f-six-months-carne-de-res">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 6 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.17.9  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-six-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-yearcarne-de-res" value="1 año" data-variation="149" data-quantity="1">
                                                <label for="f-one-yearcarne-de-res">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 1 año</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.16.9  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-year">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="feedback">* Nota. La entrega del producto es semanal, en la fecha y el horario indicado según la disponibilidad de nuestros distribuidores en cada distrito.</p>
                                    </div>
                                    <!-- Ends PriceBox -->
                                                                                                                                                <!-- Start PriceBox -->
                                    <div id="barkerPriceBox-mixto" class="filter-by-time barker-suscription-item_priceBox__disable">
                                        <div class="subtitle-xs">
                                            <h4>Elige tu suscripción</h4>
                                        </div>
                                        <div class="radio-group radio-group--check justify-content-center">
                                            <div class="radio column">
                                                                                                <input type="radio" name="f-suscription-time" id="f-one-week-mixto" value="1 semana" data-variation="159" data-quantity="1">
                                                <label for="f-one-week-mixto">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker por una semana</span>
                                                            <!-- CodeHere -->
                                                            <ul class="price">
                                                                <li>
                                                                    <span>
                                                                    S/.18 
                                                                    </span>
                                                                    <span>Por semana</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="radio-group radio-group--check owl-carousel js-suscription-type-carousel">
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-month-mixto" value="1 mes" data-variation="160" data-quantity="1">
                                                <label for="f-one-month-mixto">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker x 1 mes</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.19.5  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                      
                                                <input type="radio" name="f-suscription-time" id="f-three-months-mixto" value="3 meses" data-variation="161" data-quantity="1">
                                                <label for="f-three-months-mixto">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 3 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.18.3  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-three-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-six-months-mixto" value="6 meses" data-variation="162" data-quantity="1">
                                                <label for="f-six-months-mixto">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 6 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.17.5  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-six-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-yearmixto" value="1 año" data-variation="163" data-quantity="1">
                                                <label for="f-one-yearmixto">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 1 año</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    S/.16.5  
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-year">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="feedback">* Nota. La entrega del producto es semanal, en la fecha y el horario indicado según la disponibilidad de nuestros distribuidores en cada distrito.</p>
                                    </div>
                                    <!-- Ends PriceBox -->
                                                                                                                                                <!-- ////////////////////////////////////////////////// -->
                                    <!-- Ends Price Product-->
                                    <!-- ////////////////////////////////////////////////// --> 
                                </div>
                                <!-- ////////////////////////////////////////////////// -->
                                <!-- Ends Select Product-->
                                <!-- ////////////////////////////////////////////////// -->
                            </div>
                        </div>
                    </section>
                    <!-- ////////////////////////////////////////////////// -->
                    <!-- Step 4-->
                    <!-- ////////////////////////////////////////////////// -->
                    <h3 id="delivery-wizard-h-3" tabindex="-1" class="title current">
                        <span class="icon">
                            <svg>
                                <use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-delivery"></use>
                            </svg>
                        </span>
                        <span class="text">Pedido</span>
                    </h3>
                    <section id="delivery-wizard-p-3" role="tabpanel" aria-labelledby="delivery-wizard-h-3" class="body current" aria-hidden="false" style="display: block;">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="row">
                                    <div class="col-md-6 col-lg-5 order-1">
                                        <div class="delivery-data">
                                            <div class="text-line u-hidden-tablet">
                                                <p>Estás tan cerca de la comida real, ¡Tu perro puede olerla!</p>
                                            </div>
                                            <div class="field-group">
                                                <label>Complete sus datos para<br>comenzar su prueba</label>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-first-name" placeholder="Nombre">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-last-name" placeholder="Apellido">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-telephone" placeholder="Número de teléfono">
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <label>Dirección de envío</label>
                                                <div class="field-wrapper">
                                                    <div class="select">
                                                        <select name="f-shipping-address" id="f-shipping-address-mask">
                                                            <option value="">Distrito</option>
                                                            <option value="Barranco">Barranco</option>
                                                            <option value="Chorrillos">Chorrillos</option>
                                                            <option value="Jesús María">Jesús María</option>
                                                            <option value="La Molina">La Molina</option>
                                                            <option value="Lince">Lince</option>
                                                            <option value="Miraflores">Miraflores</option>
                                                            <option value="San Borja">San Borja</option>
                                                            <option value="San Isidro">San Isidro</option>
                                                            <option value="San Miguel">San Miguel</option>
                                                            <option value="Surco">Surco</option>
                                                            <option value="Surquillo">Surquillo</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input class="fill" type="text" name="f-info-date" id="f-info-date" value="" style="display: none;">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Horario de entrega </label>
                                                    <div class="radio-group radio-group--full row">
                                                        <div class="radio radio--small col-md-6">
                                                            <input type="radio" name="f-delivery-time" id="f-moorning-1" value="cachorro">
                                                            <label class="u-mb--lv0" for="f-moorning-1">
                                                                <div class="radio__content">
                                                                    <div class="radio__text">
                                                                        <h6>9am a 12pm</h6>
                                                                        <p>Turno día</p>
                                                                    </div>
                                                                </div>
                                                            <label class="radio__icon" for="f-moorning-1"></label>
                                                            </label>
                                                        </div>
                                                        <div class="radio radio--small col-md-6">
                                                            <input type="radio" name="f-delivery-time" id="f-afternoon-1" value="adulto">
                                                            <label class="u-mb--lv0" for="f-afternoon-1">
                                                                <div class="radio__content">
                                                                    <div class="radio__text">
                                                                        <h6>1pm a 6pm</h6>
                                                                        <p>Turno tarde</p>
                                                                    </div>
                                                                </div>
                                                            <label class="radio__icon" for="f-afternoon-1"></label>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-address-1" placeholder="Av. Jr.">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-address-2" placeholder="Urbanizado">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-address-3" placeholder="Dpto.">
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <div class="field-wrapper field-wrapper--highlight">
                                                    <div class="radio-group radio-group--square border">
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-house" value="Casa">
                                                            <label for="f-house">Casa</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-fifth" value="Quinta">
                                                            <label for="f-fifth">Quinta</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-building" value="Edificio">
                                                            <label for="f-building">Edificio</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-condominium" value="Condominio">
                                                            <label for="f-condominium">Condominio</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <div class="field-wrapper">
                                                    <label>Datos de facturación</label>
                                                    <figure><img src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/tarjetas.png" alt=""></figure>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input type="text" name="f-cardholder-name" id="f-cardholder-name" placeholder="Nombre del titular de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input type="text" name="f-cardholder-dni" id="f-cardholder-dni" placeholder="DNI del titular de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label for="f-card-number">Número de tarjeta</label>
                                                    <input type="text" name="f-card-number" id="f-card-number" placeholder="Ingrese los 14 dígitos de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Fecha de vencimiento</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="select">
                                                                <select name="f-expiration-month-card" id="f-expiration-month-card">
                                                                    <option value="">Mes</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select">
                                                                <select name="f-expiration-year-card" id="f-expiration-year-card">
                                                                    <option value="">Año</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Código de seguridad</label>
                                                    <input type="text" name="f-code-security" id="f-code-security" placeholder="CVC">
                                                </div>
                                                <!-- //BUTON PAY MOBILE -->

                                                <button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order_2" value="Realizar Pedido" data-value="Realizar Pedido">Realizar Pedido</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 offset-lg-1 order-md-1">
                                        <div class="order-detail">
                                            <div class="order-detail__head u-text-center">
                                                <div class="subtitle-xs">
                                                    <h4>Resumen del pedido</h4>
                                                </div>
                                            </div>
                                            <div class="order-detail__body">
                                                <div class="order-list">
                                                    <div class="order-item">
                                                        <div class="order-item__head u-text-center">
                                                            <h4>Caja de <span class="js-dog-name">tu mascota</span></h4>
                                                        </div>
                                                        <div class="order-item__body">
                                                            <div class="order-item__text">
                                                                <div class="order-item__text-desc">
                                                                    <h5></h5>
                                                                    <p>7 comidas de pollo</p>
                                                                    <p>Cargando y entregando semanal</p>
                                                                    <p>Todas en presentación de 1KG, contenidas en 20 hamburguesas de 50 gramos cada una.</p>
                                                                </div>
                                                                <div class="order-item__text-amount"><span></span></div>
                                                            </div>
                                                            <div class="order-item__action"><span class="link-action js-edit-order">Editar</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-action">
                                                    <p class="u-text-center"><span class="link-action js-add-dog">+ Agregar otra mascota</span></p>
                                                    <div class="modal-inner dog-new-modal" id="add-dog-new">
                                                        <div class="dog-new-modal__wrap form">
                                                            <div class="field-wrapper field-wrapper--highlight name-dog">
                                                                <label for="f-dog-name-new">¿Cómo se llama tu perro?</label>
                                                                <input type="text" name="f-dog-name-new" id="f-dog-name-new" placeholder="Ejemplo: Boby" autocomplete="off">
                                                            </div>
                                                            <div class="dog-new-modal__action">
                                                                <p>¿<span class="js-dog-name-2">Tu mascota</span> cuenta con las mismas características?</p>
                                                                <div class="dog-new-modal__action-buttons js-confirm-similar-info">
                                                                    <button class="btn btn--secondary" href="" data-similar-info="not">No</button>
                                                                    <button class="btn btn--primary" href="" data-similar-info="yes">Si</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-detail__footer">
                                                <div class="order-detail__row">
                                                    <dl>
                                                        <dt>Fecha de entrega estimada</dt>
                                                        <dd>Mié, 04/25</dd>
                                                    </dl>
                                                </div>
                                                <div class="order-detail__row">
                                                    <dl>
                                                        <dt class="barker-title-discount"></dt>
                                                        <dd><small class="old"></small> <span class="barker-price-discount"></span></dd>
                                                        <dt><small>Envío</small></dt>
                                                        <dd><small>¡GRATIS!</small></dd>
                                                        <dt>Primer pedido<br>total</dt>
                                                        <dd class="barker-precioTotal"></dd>
                                                    </dl>
                                                </div>
                                                <div class="order-detail__row u-not-border u-text-center">
                                                    <p class="feedback">Puedes cancelar la suscripción en<br>cualquier momento.</p>
                                                </div>
                                                <div class="order-detail__row u-not-border u-flex justify-content-center">
                                                    <div class="checkbox-terms u-flex">
                                                        <input type="checkbox" name="barker_terms" id="barker_terms">
                                                        <label class="icon" for="barker_terms"></label>
                                                        <label for="barker_terms">Estoy de acuerdo con los <a href="" target="_blank">términos y condiciones</a></label>
                                                    </div>
                                                </div>
                                                <div class="order-detail__row u-not-border">
                                                    <div class="coupon fields">
                                                        <input id="barker_coupon" type="text" name="discount-coupon" placeholder="Ingresar cupón de descuento">
                                                        <button id="barker_coupon_btn" class="btn btn--secondary btn--block is-disabled" type="button">Validar cupón</button>
                                                    </div>
                                                </div>
                                                <div class="order-detail__row u-not-border u-hidden-tablet">
                                                    <div class="fields">
                                                        <button id="barker_checkout_submit" class="btn btn--primary btn--block" type="submit">Realizar pedido</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>


