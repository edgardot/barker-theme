
<!-- Code Here -->
<div class="barker-checkout-header-steps wizard clearfix">
  <div class="steps clearfix">
    <ul role="tablist">
      <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
        <a id="delivery-wizard-t-0" href="#delivery-wizard-h-0" aria-controls="delivery-wizard-p-0">
          <span class="current-info audible">current step:</span>
          <span class="icon">
              <svg><use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-dog"></use></svg>
          </span>
          <span class="text">Perfil</span>
        </a>
      </li>
      <li role="tab" class="" aria-disabled="false">
        <a id="delivery-wizard-t-1" href="#delivery-wizard-h-1" aria-controls="delivery-wizard-p-1"> 
          <span class="icon">
              <svg><use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-data"></use></svg>
          </span>
          <span class="text">Datos</span>
        </a>
      </li>
      <li role="tab" class="" aria-disabled="false">
        <a id="delivery-wizard-t-2" href="#delivery-wizard-h-2" aria-controls="delivery-wizard-p-2">
          <span class="icon">
            <svg><use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-suscription"></use></svg>
          </span>
          <span class="text">Suscripción</span>
        </a>
      </li>
      <li role="tab" class=" last" aria-disabled="false">
        <a id="delivery-wizard-t-3" href="#delivery-wizard-h-3" aria-controls="delivery-wizard-p-3">
          <span class="icon">
            <svg><use xlink:href="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/sprite.svg#step-delivery"></use></svg>
          </span>
          <span class="text">Pedido</span>
        </a>
      </li>
    </ul>
  </div>
</div> 
