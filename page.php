<?php
get_header();
while ( have_posts() ) : the_post();
?>
<div class="page-wrap">
      <div class="page-name">
        <h2>Producto</h2>
      </div>
      <main class="main">
        <section class="section products">
          <div class="container">
            <div class="section-header">
              <div class="title u-text-center">
                <h2><?php the_title();?></h2>
              </div>
            </div>
            <?php the_content();?>
          </div>
        </section>
      </main>
    </div>
<?php
endwhile;
get_footer();
